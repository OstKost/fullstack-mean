import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { OverviewPage, AnalyticsPage } from '../interfaces'

@Injectable({
  providedIn: 'root'
})
export class AnalyticsService {
  constructor(private http: HttpClient) {}

  // getOverview(): Observable<OverviewPage> {
  getOverview(): OverviewPage {
    // return this.http.get<OverviewPage>('/api/analytics/overview')
    const result = <OverviewPage>{
      reven: {
        percent: 20,
        compare: 100,
        yesterday: 10,
        isHigher: true
      },
      orders: {
        percent: 20,
        compare: 100,
        yesterday: 10,
        isHigher: true
      }
    }
    return result
  }

  getAnalytics(): Observable<AnalyticsPage> {
    return this.http.get<AnalyticsPage>('/api/analytics/analytics')
  }
}
