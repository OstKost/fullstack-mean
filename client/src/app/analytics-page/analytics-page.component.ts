import { Component, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core'
import { AnalyticsService } from '../shared/services/analytics.service'
import { AnalyticsPage } from '../shared/interfaces'
import { Subscription } from 'rxjs'
import { Chart } from 'chart.js'

@Component({
  selector: 'app-analytics-page',
  templateUrl: './analytics-page.component.html',
  styleUrls: ['./analytics-page.component.css']
})
export class AnalyticsPageComponent implements AfterViewInit, OnDestroy {
  @ViewChild('reven')
  revenRef: ElementRef
  @ViewChild('order')
  orderRef: ElementRef

  oSub: Subscription
  average: number
  pending = true

  constructor(private service: AnalyticsService) {}

  ngAfterViewInit() {
    const revenConfig: any = {
      label: 'Выручка',
      color: 'rgb(255, 99, 132)'
    }
    const orderConfig: any = {
      label: 'Заказы',
      color: 'rgb(54, 162, 235)'
    }

    this.oSub = this.service.getAnalytics().subscribe((data: AnalyticsPage) => {
      this.average = data.average

      revenConfig.labels = data.chart.map(item => item.label)
      revenConfig.data = data.chart.map(item => item.reven)

      orderConfig.labels = data.chart.map(item => item.label)
      orderConfig.data = data.chart.map(item => item.order)

      const revenCtx = this.revenRef.nativeElement.getContext('2d')
      revenCtx.canvas.height = '300px'

      const orderCtx = this.orderRef.nativeElement.getContext('2d')
      orderCtx.canvas.height = '300px'

      new Chart(revenCtx, createChartConfig(revenConfig))
      new Chart(orderCtx, createChartConfig(orderConfig))

      this.pending = false
    })
  }

  ngOnDestroy() {
    if (this.oSub) {
      this.oSub.unsubscribe()
    }
  }
}

function createChartConfig({ labels, data, label, color }) {
  return {
    type: 'line',
    options: {
      responsive: true
    },
    data: {
      labels,
      datasets: [
        {
          label,
          data,
          borderColor: color,
          steppedLine: false,
          fill: false
        }
      ]
    }
  }
}
