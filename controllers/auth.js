const bcryptjs = require('bcryptjs')
const jwt = require('jsonwebtoken')
const User = require('../models/User')
const keys = require('../config/keys')
const errorHandler = require('../utils/errorHandler')

module.exports.login = async function(req, res) {
	const candidate = await User.findOne({
		email: req.body.email
	})

	if (!candidate) {
		// error if user not exist
		res.status(404).json({
			message: "User doesn't exist! Try another email."
		})
	} else {
		// login user
		const passwordResult = bcryptjs.compareSync(
			req.body.password,
			candidate.password
		)

		if (passwordResult) {
			// generate token
			const token = jwt.sign(
				{
					email: candidate.email,
					userId: candidate._id
				},
				keys.jwt,
				{ expiresIn: 60 * 60 }
			)

			res.status(200).json({ token: `Bearer ${token}` })
		} else {
			res.status(401).json({
				message: 'Passwords do not match. Try again.'
			})
		}
	}
}

module.exports.register = async function(req, res) {
	const candidate = await User.findOne({
		email: req.body.email
	})

	if (candidate) {
		// error if user exist
		res.status(409).json({
			message: 'User already exist! Try another email.'
		})
	} else {
		// create new user
		const salt = bcryptjs.genSaltSync(10)
		const password = req.body.password
		const user = new User({
			email: req.body.email,
			password: bcryptjs.hashSync(password, salt)
		})

		try {
			await user.save()
			res.status(201).json(user)
		} catch (error) {
			errorHandler(res, error)
		}
	}
}
