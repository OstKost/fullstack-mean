const moment = require('moment')
const Order = require('../models/Order')
const errorHandler = require('../utils/errorHandler')

module.exports.overview = async function(req, res) {
	try {
		const allOrders = await Order.find({ user: req.user.id }).sort({ date: 1 })
		const ordersMap = getOrdersMap(allOrders)
		const yesterdayOrders =
			ordersMap[
				moment()
					.add(-1, 'd')
					.format('DD.MM.YYYY')
			] || []

		// Количество вчерашних заказов
		const yesterdayOrdersNum = yesterdayOrders.length
		// Количество заказов
		const totalOrdersNum = allOrders.length
		// Количество дней
		const daysNum = Object.keys(ordersMap).length
		// Заказов в день
		const ordersPerDay = (totalOrdersNum / daysNum).toFixed(0)
		// Процент кол-ва заказов
		const ordersPercent = ((yesterdayOrdersNum / ordersPerDay - 1) * 100).toFixed(2)
		// Выручка общая
		const totalReven = calculatePrice(allOrders)
		// Выручка в день
		const revenPerDay = totalReven / daysNum
		// Выручка вчерашняя
		const yesterdayReven = calculatePrice(yesterdayOrders)
		// Процент выручки
		const revenPercent = ((yesterdayReven / revenPerDay - 1) * 100).toFixed(2)
		// Сравнение выручки
		const compareReven = (yesterdayReven - revenPerDay).toFixed(2)
		// Сравнение количества заказов
		const compareNum = (yesterdayOrdersNum - ordersPerDay).toFixed(2)

		res.status(200).json({
			reven: {
				percent: Math.abs(+revenPercent),
				compare: Math.abs(+compareReven),
				yesterday: +yesterdayReven,
				isHigher: +revenPercent > 0
			},
			orders: {
				percent: Math.abs(+ordersPercent),
				compare: Math.abs(+compareNum),
				yesterday: +yesterdayOrdersNum,
				isHigher: +ordersPercent > 0
			}
		})
	} catch (error) {
		errorHandler(res, error)
	}
}

module.exports.analytics = async function(req, res) {
	try {
		const allOrders = await Order.find({ user: req.user.id }).sort({ date: 1 })
		const ordersMap = getOrdersMap(allOrders)

		const average = +(calculatePrice(allOrders) / Object.keys(ordersMap).length).toFixed(2)

		const chart = Object.keys(ordersMap).map(label => {
			const reven = calculatePrice(ordersMap[label])
			const order = ordersMap[label].length

			return {
				label,
				order,
				reven
			}
		})

		res.status(200).json({ average, chart })
	} catch (error) {
		errorHandler(res, error)
	}
}

function getOrdersMap(orders = []) {
	const daysOrders = {}

	orders.forEach(order => {
		const date = moment(order.date).format('DD.MM.YYYY')
		if (date === moment().format('DD.MM.YYYY')) {
			return
		}
		if (!daysOrders[date]) {
			daysOrders[date] = []
		}
		daysOrders[date].push(order)
	})

	return daysOrders
}

function calculatePrice(orders = []) {
	return orders.reduce((total, order) => {
		const orderPrice = order.list.reduce((orderTotal, item) => {
			return (orderTotal += item.cost * item.quantity)
		}, 0)
		return (total += orderPrice)
	}, 0)
}
